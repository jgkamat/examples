
import java.util.Arrays;

class StreamExamples {
	public static void main(String args[]) {

		int[] array = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9};

		// Built in println
		System.out.println(Arrays.toString(array));

		// Loop Println
		for (int i : array) {
			System.out.print(i + " ");
		}
		System.out.println();

		// Stream println
		Arrays.stream(array)
			.forEach((input) -> System.out.print(input + " "));
		System.out.println();

		// Stream -> Multiply By 10 -> Println
		Arrays.stream(array)
			.map((input) -> input * 10)
			.forEach((input) -> System.out.print(input + " "));
		System.out.println();

		// Stream -> Multiply by 10 -> Select only < 50 -> println
		Arrays.stream(array)
			.map((input) -> input * 10)
			.filter((input) -> input < 50)
			.forEach((input) -> System.out.print(input + " "));
		System.out.println();

		// Stream -> Multiply by 10 -> Select only < 50 -> Multiply by a random # -> Println
		Arrays.stream(array)
			.map((input) -> input * 10)
			.filter((input) -> input < 50)
			.map((input) -> (int) (input * Math.random()))
			.forEach((input) -> System.out.print(input + " "));
		System.out.println();

		// Stream -> Multiply by 10 -> Select only < 50 -> Multiply by a random # -> Add up Elements -> Println the sum!
		int result = Arrays.stream(array)
			.map((input) -> input * 10)
			.filter((input) -> input < 50)
			.map((input) -> (int) (input * Math.random()))
			.reduce((input1, input2) -> input1 + input2).getAsInt();
		System.out.println(result);
	}
}
